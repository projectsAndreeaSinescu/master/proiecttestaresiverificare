// This is a mutant program.
// Author : ysma

package mujava_app;


import java.util.*;


public class MujavaApp
{

    public static final java.lang.String INVALID_SIZE_MESSAGE = "Sirul trebuie sa contina minim un numar si maxim 10 numere";

    public static final java.lang.String INVALID_NUMBER_IN_ARRAY_MESSAGE = "Numerele introduse trebuie sa fie naturale si de maxim 5 cifre";

    public static  java.lang.Boolean isPrimeNumber( java.lang.Integer number )
    {
        if (number == 0 || number == 1) {
            return false;
        } else {
            for (int divisor = 2; divisor <= Math.sqrt( number ); divisor++) {
                if (number % divisor == 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public static  java.lang.String palindromArrayOfPrimeNumbers( java.util.List<Integer> list )
    {
        java.lang.Integer arraySize = list.size();
        if (arraySize == 0 || arraySize > 10) {
            return INVALID_SIZE_MESSAGE;
        } else {
            for (int i = 0; i < arraySize; i++) {
                if (list.get( i ) < 0 || list.get( ++i ) > 99999) {
                    return INVALID_NUMBER_IN_ARRAY_MESSAGE;
                }
            }
            java.lang.String isPalindromArray = "1";
            java.lang.String hasOnlyPrimeNumbers = "1";
            java.lang.Integer[] frequencyOfElements = Collections.nCopies( 100000, 0 ).toArray( new java.lang.Integer[100000] );
            for (int i = 0; i < arraySize; i++) {
                frequencyOfElements[list.get( i )]++;
            }
            java.lang.Boolean existOddFrequency = false;
            for (int i = 0; i < frequencyOfElements.length; i++) {
                if (frequencyOfElements[i] % 2 != 0) {
                    if (existOddFrequency) {
                        isPalindromArray = "0";
                        break;
                    } else {
                        existOddFrequency = true;
                    }
                }
            }
            for (int i = 0; i < arraySize; i++) {
                java.lang.Boolean isPrime = isPrimeNumber( list.get( i ) );
                if (!isPrime) {
                    hasOnlyPrimeNumbers = "0";
                    break;
                }
            }
            return isPalindromArray + hasOnlyPrimeNumbers;
        }
    }

}
