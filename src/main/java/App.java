import lombok.Generated;

import java.util.*;

public class App {

    public static final String INVALID_SIZE_MESSAGE = "Sirul trebuie sa contina minim un numar si maxim 10 numere";
    public static final String INVALID_NUMBER_IN_ARRAY_MESSAGE = "Numerele introduse trebuie sa fie naturale si de maxim 5 cifre";
    public static final String INVALID_INPUT_MESSAGE = "Intrarea este invalida";

    @Generated
    public static void main(String[] args) {
        Scanner in = null;

        try {
            in = new Scanner(System.in);
            Integer arraySize = in.nextInt();
            List<Integer> list = new ArrayList<Integer>();

            for(int i = 0; i < arraySize; i++) {
                list.add(in.nextInt());
            }

            System.out.println(palindromArrayOfPrimeNumbers(list));
        } catch(Exception e) {
            if (e instanceof InputMismatchException) {
                System.out.println(INVALID_INPUT_MESSAGE);
            }
        } finally {
            in.close();
        }
    }

    public static Boolean isPrimeNumber(Integer number) {
        if (number == 0 || number == 1) {
            return false;
        } else {
            for (int divisor = 2; divisor <= Math.sqrt(number); divisor++) {
                if (number % divisor == 0) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Se citeste un sir de numere naturale de lungime maxima 10 si nevid (lungime >= 1), de maxim 5 cifre.
     * - daca prin rearanjarea elementelor din sir se poate obtine un sir palindrom
     *   si toate numerele din sir sunt prime, se va returna “11”
     * - daca prin rearanjarea elementelor din sir se poate obtine un sir palindrom
     *   si exista cel putin un numar care nu este prim in sir, se va returna “10”
     * - daca prin rearanjarea elementelor din sir nu se poate obtine un sir palindrom
     *   si toate numerele din sir sunt prime, se va returna “01”
     * - daca prin rearanjarea elementelor din sir nu se poate obtine un sir palindrom
     *   si exista cel putin un numar care nu este prim in sir, se va returna “00”
     * Pentru cazurile de eroare se va returna un mesaj corespunzator.
     *
     * @param list sirul de numere
     * @return rezultatul conform cerintei de mai sus
     */
    public static String palindromArrayOfPrimeNumbers(List<Integer> list) {
        Integer arraySize = list.size();

        if (arraySize == 0 || arraySize > 10) {
            return INVALID_SIZE_MESSAGE;
        } else {
            for (Integer number : list) {
                if (number < 0 || number > 99999) {
                    return INVALID_NUMBER_IN_ARRAY_MESSAGE;
                }
            }

            String isPalindromArray = "1";
            String hasOnlyPrimeNumbers = "1";
            HashMap<Integer, Integer> frequencyOfElements = new HashMap<Integer, Integer>();

            for (Integer number : list) {
                if (frequencyOfElements.containsKey(number)) {
                    frequencyOfElements.put(number, frequencyOfElements.get(number) + 1);
                } else {
                    frequencyOfElements.put(number, 1);
                }
            }

            Boolean existOddFrequency = false;

            for (Integer frequency : frequencyOfElements.values()) {
                if (frequency % 2 != 0) {
                    if (existOddFrequency) {
                        isPalindromArray = "0";
                        break;
                    } else {
                        existOddFrequency = true;
                    }
                }
            }

            for (Integer number : list) {
                Boolean isPrime = isPrimeNumber(number);

                if (!isPrime) {
                    hasOnlyPrimeNumbers = "0";
                    break;
                }
            }

            return isPalindromArray + hasOnlyPrimeNumbers;
        }
    }
}
