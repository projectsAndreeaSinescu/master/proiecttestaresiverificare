package mujava_app;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class MujavaAppCauseEffectGraphingTest {

    @Test
    public void causeEffectGraphing() {
        // prima coloana
        String palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14));
        Assert.assertEquals(palindromArrayOfPrimeNumbersResult, MujavaApp.INVALID_SIZE_MESSAGE);

        // a doua coloana
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(1, 2, 3, -400, 5));
        Assert.assertEquals(palindromArrayOfPrimeNumbersResult, MujavaApp.INVALID_NUMBER_IN_ARRAY_MESSAGE);

        // a treia coloana
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(7, 7, 2, 2, 3, 5, 5));
        assertEquals(palindromArrayOfPrimeNumbersResult, "11");

        // a patra coloana
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(1, 1, 3, 5, 5));
        assertEquals(palindromArrayOfPrimeNumbersResult, "10");

        // a cincea coloana
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(29, 29, 3, 5, 5, 5));
        assertEquals(palindromArrayOfPrimeNumbersResult, "01");

        // a sasea coloana
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(29, 28, 3, 5, 5, 5));
        assertEquals(palindromArrayOfPrimeNumbersResult, "00");
    }

    @Test
    public void kill_AOIS_9() {
        String palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(100000, 2, 3));
        assertEquals(palindromArrayOfPrimeNumbersResult, MujavaApp.INVALID_NUMBER_IN_ARRAY_MESSAGE);
    }
}