package mujava_app;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class MujavaAppPartitioningEquivalenceTest {
    
    @Test
    public void partitioningEquivalence() {
        // C_111 = N_1 && P_1 && C_1(sir)
        String palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(2,2,3,2,3));
        assertEquals(palindromArrayOfPrimeNumbersResult, "11");

        // C_112 = N_1 && P_1 && C_2(sir)
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(2,2,3,1,2,2,3));
        assertEquals(palindromArrayOfPrimeNumbersResult, "10");

        // C_113 = N_1 && P_1 && C_3(sir)
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(11,29,23));
        assertEquals(palindromArrayOfPrimeNumbersResult, "01");
        // C_114 = N_1 && P_1 && C_4(sir)
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(4,6,25));
        assertEquals(palindromArrayOfPrimeNumbersResult, "00");

        // C_12 = N_1 && P_2
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(-121,29,23));
        Assert.assertEquals(palindromArrayOfPrimeNumbersResult, MujavaApp.INVALID_NUMBER_IN_ARRAY_MESSAGE);

        // C_13 = N_1 && P_3
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(121,29,233333));
        Assert.assertEquals(palindromArrayOfPrimeNumbersResult, MujavaApp.INVALID_NUMBER_IN_ARRAY_MESSAGE);

        // C_2 = N_2
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(new ArrayList<Integer>());
        Assert.assertEquals(palindromArrayOfPrimeNumbersResult, MujavaApp.INVALID_SIZE_MESSAGE);

        // C_3 = N_3
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Collections.nCopies(20, 5));
        Assert.assertEquals(palindromArrayOfPrimeNumbersResult, MujavaApp.INVALID_SIZE_MESSAGE);
    }

    @Test
    public void kill_AOIS_1() {
        String palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(8));
        assertEquals(palindromArrayOfPrimeNumbersResult, "10");
    }
}