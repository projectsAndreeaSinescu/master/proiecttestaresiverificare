package mujava_app;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class MujavaAppBoundaryValuesTest {

    @Test
    public void boundaryValues() {
        // N_1 = 1..10 => 1, 10 (valori de frontiera) si o valoare din interior
        String palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(2));
        assertEquals(palindromArrayOfPrimeNumbersResult, "11");

        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Collections.nCopies(10, 1));
        assertEquals(palindromArrayOfPrimeNumbersResult, "10");

        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(1,2,3,4));
        assertEquals(palindromArrayOfPrimeNumbersResult, "00");

        // N_2 = {n | n == 0} => 0 (valoarea de frontiera)
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList());
        Assert.assertEquals(palindromArrayOfPrimeNumbersResult, MujavaApp.INVALID_SIZE_MESSAGE);

        // N_3 = {n | n > 10} => 11 (valoare de frontiera) si o valoare din interior
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Collections.nCopies(11, 7));
        Assert.assertEquals(palindromArrayOfPrimeNumbersResult, MujavaApp.INVALID_SIZE_MESSAGE);

        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Collections.nCopies(100, 3));
        Assert.assertEquals(palindromArrayOfPrimeNumbersResult, MujavaApp.INVALID_SIZE_MESSAGE);

        // P_1 = { oricare ar fi nr care apartine sirului | nr >= 0 si nr <= 99999 } =>
        // - sirul este format din numarul 0
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(0));
        assertEquals(palindromArrayOfPrimeNumbersResult, "10");

        // - sirul este format din numarul 99999
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(99999));
        assertEquals(palindromArrayOfPrimeNumbersResult, "10");

        // - sirul este format dintr-o valoare din mijloc
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(4564));
        assertEquals(palindromArrayOfPrimeNumbersResult, "10");

        // P_2 = { exista nr care apartine sirului | nr < 0 }
        // - sirul contine pe prima pozitie -1
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(-1, 2, 2));
        Assert.assertEquals(palindromArrayOfPrimeNumbersResult, MujavaApp.INVALID_NUMBER_IN_ARRAY_MESSAGE);

        // - sirul contine pe ultima pozitie -1
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(2, 2, -1));
        Assert.assertEquals(palindromArrayOfPrimeNumbersResult, MujavaApp.INVALID_NUMBER_IN_ARRAY_MESSAGE);

        // - sirul contine undeva in mijloc -1
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(2, -1, 2));
        Assert.assertEquals(palindromArrayOfPrimeNumbersResult, MujavaApp.INVALID_NUMBER_IN_ARRAY_MESSAGE);

        // - sirul contine pe prima pozitie o valoare din interiorul multimii
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(-36726367, 2, 2));
        Assert.assertEquals(palindromArrayOfPrimeNumbersResult, MujavaApp.INVALID_NUMBER_IN_ARRAY_MESSAGE);

        // - sirul contine pe ultima pozitie o valoare din interiorul multimii
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(2, 2, -17456));
        Assert.assertEquals(palindromArrayOfPrimeNumbersResult, MujavaApp.INVALID_NUMBER_IN_ARRAY_MESSAGE);

        // - sirul contine undeva in mijloc o valoare din interiorul multimii
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(2, -33787, 2));
        Assert.assertEquals(palindromArrayOfPrimeNumbersResult, MujavaApp.INVALID_NUMBER_IN_ARRAY_MESSAGE);

        // P_3 = { exista nr care apartine sirului | nr > 99999 }
        // - sirul contine pe prima pozitie 100000
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(100000, 2, 2));
        Assert.assertEquals(palindromArrayOfPrimeNumbersResult, MujavaApp.INVALID_NUMBER_IN_ARRAY_MESSAGE);

        // - sirul contine pe ultima pozitie 100000
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(2, 2, 100000));
        Assert.assertEquals(palindromArrayOfPrimeNumbersResult, MujavaApp.INVALID_NUMBER_IN_ARRAY_MESSAGE);

        // - sirul contine undeva in mijloc 100000
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(2, 100000, 2));
        Assert.assertEquals(palindromArrayOfPrimeNumbersResult, MujavaApp.INVALID_NUMBER_IN_ARRAY_MESSAGE);

        // - sirul contine pe prima pozitie o valoare din interiorul multimii
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(36726367, 2, 2));
        Assert.assertEquals(palindromArrayOfPrimeNumbersResult, MujavaApp.INVALID_NUMBER_IN_ARRAY_MESSAGE);

        // - sirul contine pe ultima pozitie o valoare din interiorul multimii
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(2, 2, 17456746));
        Assert.assertEquals(palindromArrayOfPrimeNumbersResult, MujavaApp.INVALID_NUMBER_IN_ARRAY_MESSAGE);

        // - sirul contine undeva in mijloc o valoare din interiorul multimii
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(2, 33787643, 2));
        Assert.assertEquals(palindromArrayOfPrimeNumbersResult, MujavaApp.INVALID_NUMBER_IN_ARRAY_MESSAGE);

        // C_1(sir) = sirul poate fi palindrom si contine doar numere prime => sir cu numere identice
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Collections.nCopies(9, 29));
        assertEquals(palindromArrayOfPrimeNumbersResult, "11");

        // C_2(sir) = sirul poate fi palindrom si exista cel putin un numar care nu este prim in sir
        // - sirul contine doar 1-uri
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Collections.nCopies(8, 1));
        assertEquals(palindromArrayOfPrimeNumbersResult, "10");

        // - sirul contine doar 0-uri
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Collections.nCopies(5, 0));
        assertEquals(palindromArrayOfPrimeNumbersResult, "10");

        // - sir cu numere identice diferite de 1 si 0
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Collections.nCopies(8, 24));
        assertEquals(palindromArrayOfPrimeNumbersResult, "10");

        // - numarul care nu este prim se afla pe prima pozitie
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(28, 3, 5, 3, 5));
        assertEquals(palindromArrayOfPrimeNumbersResult, "10");

        // - numarul care nu este prim se afla pe ultima pozitie
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(3, 5, 3, 5, 28));
        assertEquals(palindromArrayOfPrimeNumbersResult, "10");

        // - numarul care nu este prim se afla undeva la mijloc
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(3, 5, 3, 28, 5));
        assertEquals(palindromArrayOfPrimeNumbersResult, "10");

        // C_3(sir) = sirul nu poate fi palindrom si contine doar numere prime => nu exista frontiere clare

        // C_4(sir) = sirul nu poate fi palindrom si exista cel putin un numar care nu este prim in sir
        // - numarul care nu este prim se afla pe prima pozitie
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(28, 3, 7, 3, 5));
        assertEquals(palindromArrayOfPrimeNumbersResult, "00");

        // - numarul care nu este prim se afla pe ultima pozitie
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(3, 5, 3, 7, 28));
        assertEquals(palindromArrayOfPrimeNumbersResult, "00");

        // - numarul care nu este prim se afla undeva la mijloc
        palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(3, 7, 3, 28, 5));
        assertEquals(palindromArrayOfPrimeNumbersResult, "00");
    }

    @Test
    public void AOIS_25() {
        String palindromArrayOfPrimeNumbersResult = MujavaApp.palindromArrayOfPrimeNumbers(Arrays.asList(0, 0, 0, 2, 2, 2));
        assertEquals(palindromArrayOfPrimeNumbersResult, "00");
    }
}