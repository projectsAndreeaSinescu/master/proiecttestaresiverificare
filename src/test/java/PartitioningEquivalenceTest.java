import static org.junit.Assert.*;

import org.junit.Test;

import java.util.Collections;
import java.util.List;

public class PartitioningEquivalenceTest {

    @Test
    public void partitioningEquivalence() {
        // C_111 = N_1 && P_1 && C_1(sir)
        String palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(2,2,3,2,3));
        assertEquals(palindromArrayOfPrimeNumbersResult, "11");

        // C_112 = N_1 && P_1 && C_2(sir)
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(2,2,3,1,2,2,3));
        assertEquals(palindromArrayOfPrimeNumbersResult, "10");

        // C_113 = N_1 && P_1 && C_3(sir)
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(11,29,23));
        assertEquals(palindromArrayOfPrimeNumbersResult, "01");
        // C_114 = N_1 && P_1 && C_4(sir)
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(4,6,25));
        assertEquals(palindromArrayOfPrimeNumbersResult, "00");

        // C_12 = N_1 && P_2
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(-121,29,23));
        assertEquals(palindromArrayOfPrimeNumbersResult, App.INVALID_NUMBER_IN_ARRAY_MESSAGE);

        // C_13 = N_1 && P_3
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(121,29,233333));
        assertEquals(palindromArrayOfPrimeNumbersResult, App.INVALID_NUMBER_IN_ARRAY_MESSAGE);

        // C_2 = N_2
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of());
        assertEquals(palindromArrayOfPrimeNumbersResult, App.INVALID_SIZE_MESSAGE);

        // C_3 = N_3
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(Collections.nCopies(20, 5));
        assertEquals(palindromArrayOfPrimeNumbersResult, App.INVALID_SIZE_MESSAGE);
    }
}
