import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class MCDCTest {

    @Test
    public void modifiedConditionDecisionCoverage() {

        /**
         * Pentru ramura conditionala: arraySize == 0 || arraySize > 10 (linia 2)
         *      => C1 v C2, unde C1 = arraySize == 0 si C2 = arraySize > 10
         */

        // t1: C1 == true, C2 == false => C1 v C2 == true
        String palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of());
        assertEquals(palindromArrayOfPrimeNumbersResult, App.INVALID_SIZE_MESSAGE);

        // t2: C1 == false, C2 == false => C1 v C2 == false
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(2, 2, 3, 3, 5, 5, 7, 7, 7));
        assertEquals(palindromArrayOfPrimeNumbersResult, "11");

        // t3: C1 == false, C2 == true => C1 v C2 == true
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(2, 2, 3, 3, 5, 5, 7, 7, 7, 11, 11, 13, 13, 13));
        assertEquals(palindromArrayOfPrimeNumbersResult, App.INVALID_SIZE_MESSAGE);

        /**
         * Pentru for-ul de la linia 5, singura varianta posibila este sa treaca cel putin
         * o data prin el, deoarece sirul contine cel putin un numar.
         *      => C3, unde C3 = sirul contine cel putin nu numar
         */

        // t4: C3 == true
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(2, 2, 3, 3, 5, 5, 7, 7, 7, -8));
        assertEquals(palindromArrayOfPrimeNumbersResult, App.INVALID_NUMBER_IN_ARRAY_MESSAGE);

        /**
         * Pentru ramura conditionala: number < 0 || number > 99999 (linia 6)
         *      => C4 v C5, unde C4 = number < 0 si C5 = number > 99999
         */

        // t5: C4 == true, C5 == false => C4 v C5 == true
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(-398));
        assertEquals(palindromArrayOfPrimeNumbersResult, App.INVALID_NUMBER_IN_ARRAY_MESSAGE);

        // t6: C4 == false, C5 == false => C4 v C5 == false
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(2, 2, 7, 8, 7));
        assertEquals(palindromArrayOfPrimeNumbersResult, "10");

        // t7: C4 == false, C5 == true => C4 v C5 == true
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(222222, 222222, 3, 3, 5, 5));
        assertEquals(palindromArrayOfPrimeNumbersResult, App.INVALID_NUMBER_IN_ARRAY_MESSAGE);

        /**
         * Pentru for-ul de la linia 11, singura varianta posibila este sa treaca cel putin
         * o data prin el, deoarece sirul contine cel putin un numar.
         *      => C6, unde C6 = sirul contine cel putin nu numar.
         */

        // t8: C6 == true
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(2, 2, 3, 3, 5, 5, 5, 7, 7, 7));
        assertEquals(palindromArrayOfPrimeNumbersResult, "01");

        /**
         * Pentru ramura conditionala: frequencyOfElements.containsKey(number) (linia 12)
         *      => C7, unde C7 = frequencyOfElements.containsKey(number) (vectorul de frecventa contine deja cheia)
         */

        // t9: C7 == true
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(2, 2));
        assertEquals(palindromArrayOfPrimeNumbersResult, "11");

        // t10: C7 == false
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(111));
        assertEquals(palindromArrayOfPrimeNumbersResult, "10");

        /**
         * Pentru for-ul de la linia 17, singura varianta posibila este sa treaca cel putin
         * o data prin el, deoarece sirul contine cel putin un numar, deci si vectorul de
         * frecventa va contine cel putin o cheie.
         *      => C8, unde C8 = vectorul de frecventa contine cel putin o cheie
         */

        // t11: C8 == true
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(1111));
        assertEquals(palindromArrayOfPrimeNumbersResult, "10");

        /**
         * Pentru ramura conditionala: frequency % 2 != 0 (linia 18)
         *      => C9, unde C9 = frequency % 2 != 0 (frecventa este un numar impar)
         */

        // t12: C9 == true
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(3, 3, 3));
        assertEquals(palindromArrayOfPrimeNumbersResult, "11");

        // t13: C9 == false
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(3, 3));
        assertEquals(palindromArrayOfPrimeNumbersResult, "11");

        /**
         * Pentru ramura conditionala: existOddFrequency (linia 19)
         *      => C10, unde C10 = existOddFrequency (a mai fost gasita inainte o frecventa impara)
         */

        // t14: C10 == true
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(2, 3));
        assertEquals(palindromArrayOfPrimeNumbersResult, "01");

        // t15: C10 == false
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(2, 3, 2));
        assertEquals(palindromArrayOfPrimeNumbersResult, "11");

        /**
         * Pentru for-ul de la linia 24, singura varianta posibila este sa treaca cel putin
         * o data prin el, deoarece sirul contine cel putin un numar.
         *      => C11, unde C11 = sirul contine cel putin nu numar
         */

        // t16: C11 == true
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(53));
        assertEquals(palindromArrayOfPrimeNumbersResult, "11");

        /**
         * Pentru ramura conditionala: !isPrime (linia 26)
         *      => ¬ C12, unde C12 = isPrime (arata daca numarul este prim sau nu).
         */

        // t17: C12 == true => ¬ C12 == false
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(59));
        assertEquals(palindromArrayOfPrimeNumbersResult, "11");

        // t18: C12 == false => ¬ C12 == true
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(66));
        assertEquals(palindromArrayOfPrimeNumbersResult, "10");

        /**
         * Pentru ramura conditionala: number == 0 || number == 1 (linia 30)
         *      => C13 v C14, unde C13 = number == 0 si C14 = number == 1
         */

        // t19: C13 == true, C14 == false => C13 v C14 == true
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(0));
        assertEquals(palindromArrayOfPrimeNumbersResult, "10");

        // t20: C13 == false, C14 == false => C13 v C14 == false
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(2));
        assertEquals(palindromArrayOfPrimeNumbersResult, "11");

        // t21: C13 == false, C14 == true => C13 v C14 == true
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(1));
        assertEquals(palindromArrayOfPrimeNumbersResult, "10");

        /**
         * Pentru for-ul de la linia 33
         *      => C15, unde C15 = radicalul numarului >= 2
         */

        // t22: C15 == true
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(3));
        assertEquals(palindromArrayOfPrimeNumbersResult, "11");

        // t23: C15 == false
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(25));
        assertEquals(palindromArrayOfPrimeNumbersResult, "10");

        /**
         * Pentru ramura conditionala: number % divisor == 0 (linia 34)
         *      => C16, unde C16 = number % divisor == 0
         */

        // t24: C16 == true
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(4));
        assertEquals(palindromArrayOfPrimeNumbersResult, "10");

        // t25: C16 == false
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(5));
        assertEquals(palindromArrayOfPrimeNumbersResult, "11");
    }
}