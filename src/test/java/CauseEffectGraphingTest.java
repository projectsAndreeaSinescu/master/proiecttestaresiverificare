import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class CauseEffectGraphingTest {

    @Test
    public void causeEffectGraphing() {
        // prima coloana
        String palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14));
        assertEquals(palindromArrayOfPrimeNumbersResult, App.INVALID_SIZE_MESSAGE);

        // a doua coloana
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(1, 2, 3, -400, 5));
        assertEquals(palindromArrayOfPrimeNumbersResult, App.INVALID_NUMBER_IN_ARRAY_MESSAGE);

        // a treia coloana
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(7, 7, 2, 2, 3, 5, 5));
        assertEquals(palindromArrayOfPrimeNumbersResult, "11");

        // a patra coloana
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(1, 1, 3, 5, 5));
        assertEquals(palindromArrayOfPrimeNumbersResult, "10");

        // a cincea coloana
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(29, 29, 3, 5, 5, 5));
        assertEquals(palindromArrayOfPrimeNumbersResult, "01");

        // a sasea coloana
        palindromArrayOfPrimeNumbersResult = App.palindromArrayOfPrimeNumbers(List.of(29, 28, 3, 5, 5, 5));
        assertEquals(palindromArrayOfPrimeNumbersResult, "00");
    }
}